package cooper.cooperballback;

import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;

import br.config.security.jwt.AccountCredentials;
import gm.utils.rest.URest;

public class TesteLogin {

	public static void main(String[] args) {

		Map<String, String> headers = new HashedMap<>();
		
		AccountCredentials dados = new AccountCredentials();
		dados.setUsername("user@gmail.com");
		dados.setPassword("123456");
		
		URest post = URest.post("http://localhost:8080/login/signin", headers, dados);

		String authorization = post.getAuthorization();
		headers.put("authorization", authorization);
		
	}

}
