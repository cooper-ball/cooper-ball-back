package br.config.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import br.cooper.infra.outros.ThreadScopeStart;
import gm.utils.number.UInteger;

@Component
public class JWTAuthenticationFilter extends GenericFilterBean {

	@Autowired ThreadScopeStart startThreadScope;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		Authentication authentication = TokenAuthenticationService.getAuthentication((HttpServletRequest) request);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		if (authentication != null) {
			
			HttpServletRequest req = (HttpServletRequest) request;
			String uri = req.getRequestURI();
			
			if (!"/error".contentEquals(uri)) {
				Integer sessao = UInteger.toInt(authentication.getPrincipal());
				startThreadScope.start(sessao, uri);
			}
			
		}
		
		filterChain.doFilter(request, response);
	}

}