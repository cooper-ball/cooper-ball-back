package br.cooper.infra.abstracoes;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import br.cooper.infra.models.AuditoriaTransacao;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.AuditoriaTransacaoSelect;
import br.cooper.infra.services.ComandoService;
import br.cooper.infra.services.LoginService;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;

public abstract class AuditoriaTransacaoServiceAbstract extends ServiceModelo<AuditoriaTransacao> {

	@Autowired protected LoginService loginService;
	@Autowired protected ComandoService comandoService;

	@Override
	public Class<AuditoriaTransacao> getClasse() {
		return AuditoriaTransacao.class;
	}

	@Override
	public MapSO toMap(AuditoriaTransacao o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected AuditoriaTransacao fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		AuditoriaTransacao o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setComando(find(comandoService, mp, "comando"));
		o.setData(mp.get("data"));
		o.setLogin(find(loginService, mp, "login"));
		o.setTempo(mp.getBigDecimal("tempo", 3));
		return o;
	}

	@Override
	public AuditoriaTransacao newO() {
		AuditoriaTransacao o = new AuditoriaTransacao();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(AuditoriaTransacao o) {
		if (o.getComando() == null) {
			throw new MessageException("O campo Auditoria Transação > Comando é obrigatório");
		}
		if (o.getData() == null) {
			throw new MessageException("O campo Auditoria Transação > Data é obrigatório");
		}
		if (o.getLogin() == null) {
			throw new MessageException("O campo Auditoria Transação > Login é obrigatório");
		}
		if (o.getTempo() == null) {
			throw new MessageException("O campo Auditoria Transação > Tempo é obrigatório");
		}
		o.setTempo(validaDecimal(o.getTempo(), 8, 3, false));
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueDataLogin(o);
		}
		validar2(o);
		validar3(o);
	}

	public void validarUniqueDataLogin(AuditoriaTransacao o) {
		AuditoriaTransacaoSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.data().eq(o.getData());
		select.login().eq(o.getLogin());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("auditoriatransacao_data_login"));
		}
	}

	protected void validar2(AuditoriaTransacao o) {}

	protected void validar3(AuditoriaTransacao o) {}

	@Transactional
	public AuditoriaTransacao bloqueiaRegistro(AuditoriaTransacao o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, AuditoriaTransacao> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		AuditoriaTransacaoSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<AuditoriaTransacao> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public AuditoriaTransacaoSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		AuditoriaTransacaoSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.fk(params, "comando", select.comando());
		FiltroConsulta.dateTime(params, "data", select.data());
		FiltroConsulta.fk(params, "login", select.login());
		FiltroConsulta.decimal(params, "tempo", select.tempo());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected AuditoriaTransacao buscaUnicoObrig(MapSO params) {
		AuditoriaTransacaoSelect<?> select = selectBase(null, null, params);
		AuditoriaTransacao o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um AuditoriaTransacao com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected AuditoriaTransacao setOld(AuditoriaTransacao o) {
		AuditoriaTransacao old = newO();
		old.setComando(o.getComando());
		old.setData(o.getData());
		old.setLogin(o.getLogin());
		old.setTempo(o.getTempo());
		o.setOld(old);
		return o;
	}

	public AuditoriaTransacaoSelect<?> select(Boolean excluido) {
		AuditoriaTransacaoSelect<?> o = new AuditoriaTransacaoSelect<AuditoriaTransacaoSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public AuditoriaTransacaoSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(AuditoriaTransacao o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(AuditoriaTransacao o) {
		if (Null.is(o)) {
			return null;
		}
		return comandoService.getText(o.getComando());
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("AuditoriaTransacao");
		list.add("login;comando;data;tempo");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("auditoriatransacao_data_login", "A combinação de campos Data + Login não pode se repetir. Já existe um registro com esta combinação.");
	}
}
