package br.cooper.infra.abstracoes;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import br.cooper.infra.models.Observacao;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.ObservacaoSelect;
import br.cooper.infra.services.ArquivoService;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;

public abstract class ObservacaoServiceAbstract extends ServiceModelo<Observacao> {

	@Autowired protected ArquivoService arquivoService;

	@Override
	public Class<Observacao> getClasse() {
		return Observacao.class;
	}

	@Override
	public MapSO toMap(Observacao o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getAnexo() != null) {
			map.put("anexo", arquivoService.toMap(o.getAnexo(), listas));
		}
		if (o.getTexto() != null) {
			map.put("texto", o.getTexto());
		}
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected Observacao fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		Observacao o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setAnexo(arquivoService.get(mp.get("anexo")));
		o.setTexto(mp.getString("texto"));
		return o;
	}

	@Override
	public Observacao newO() {
		Observacao o = new Observacao();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(Observacao o) {
		o.setTexto(tratarString(o.getTexto()));
		if (o.getTexto() == null) {
			throw new MessageException("O campo Observação > Texto é obrigatório");
		}
		if (StringLength.get(o.getTexto()) > 500) {
			throw new MessageException("O campo Observação > Texto aceita no máximo 500 caracteres");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueTextoEntidadeRegistro(o);
		}
		validar2(o);
		if (o.getEntidade() == null) {
			throw new MessageException("O campo Observação > Entidade é obrigatório");
		}
		if (o.getRegistro() == null) {
			throw new MessageException("O campo Observação > Registro é obrigatório");
		}
		validar3(o);
	}

	public void validarUniqueTextoEntidadeRegistro(Observacao o) {
		ObservacaoSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.texto().eq(o.getTexto());
		select.entidade().eq(o.getEntidade());
		select.registro().eq(o.getRegistro());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("observacao_texto_entidade_registro"));
		}
	}

	protected void validar2(Observacao o) {}

	protected void validar3(Observacao o) {}

	@Transactional
	public Observacao bloqueiaRegistro(Observacao o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	public ResultadoConsulta consulta(MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, Observacao> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		ObservacaoSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<Observacao> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public ObservacaoSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		ObservacaoSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.fk(params, "anexo", select.anexo());
		FiltroConsulta.fk(params, "entidade", select.entidade());
		FiltroConsulta.integer(params, "registro", select.registro());
		FiltroConsulta.string(params, "texto", select.texto());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected Observacao buscaUnicoObrig(MapSO params) {
		ObservacaoSelect<?> select = selectBase(null, null, params);
		Observacao o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um Observacao com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected Observacao setOld(Observacao o) {
		Observacao old = newO();
		old.setAnexo(o.getAnexo());
		old.setEntidade(o.getEntidade());
		old.setRegistro(o.getRegistro());
		old.setTexto(o.getTexto());
		o.setOld(old);
		return o;
	}

	public ObservacaoSelect<?> select(Boolean excluido) {
		ObservacaoSelect<?> o = new ObservacaoSelect<ObservacaoSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public ObservacaoSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(Observacao o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(Observacao o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getTexto();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Observacao");
		list.add("texto;anexo.nome");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("observacao_texto_entidade_registro", "A combinação de campos Texto + Entidade + Registro não pode se repetir. Já existe um registro com esta combinação.");
	}
}
