package br.cooper.infra.abstracoes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.cooper.infra.models.AuditoriaEntidade;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.AuditoriaEntidadeSelect;
import br.cooper.infra.services.AuditoriaTransacaoService;
import br.cooper.infra.services.EntidadeService;
import br.cooper.infra.services.TipoAuditoriaEntidadeService;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import src.commom.utils.object.Null;

public abstract class AuditoriaEntidadeServiceAbstract extends ServiceModelo<AuditoriaEntidade> {

	@Autowired protected EntidadeService entidadeService;
	@Autowired protected AuditoriaTransacaoService auditoriaTransacaoService;
	@Autowired protected TipoAuditoriaEntidadeService tipoAuditoriaEntidadeService;

	@Override
	public Class<AuditoriaEntidade> getClasse() {
		return AuditoriaEntidade.class;
	}

	@Override
	public MapSO toMap(AuditoriaEntidade o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		return map;
	}

	@Override
	protected AuditoriaEntidade fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		AuditoriaEntidade o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setEntidade(find(entidadeService, mp, "entidade"));
		o.setNumeroDaOperacao(mp.getInt("numeroDaOperacao"));
		o.setRegistro(mp.getInt("registro"));
		o.setTipo(findId(tipoAuditoriaEntidadeService, mp, "tipo"));
		o.setTransacao(find(auditoriaTransacaoService, mp, "transacao"));
		return o;
	}

	@Override
	public AuditoriaEntidade newO() {
		AuditoriaEntidade o = new AuditoriaEntidade();
		return o;
	}

	@Override
	protected final void validar(AuditoriaEntidade o) {
		if (o.getEntidade() == null) {
			throw new MessageException("O campo Auditoria Entidade > Entidade é obrigatório");
		}
		if (o.getNumeroDaOperacao() == null) {
			throw new MessageException("O campo Auditoria Entidade > Número da Operação é obrigatório");
		}
		if (o.getRegistro() == null) {
			throw new MessageException("O campo Auditoria Entidade > Registro é obrigatório");
		}
		if (o.getTipo() == null) {
			throw new MessageException("O campo Auditoria Entidade > Tipo é obrigatório");
		}
		if (o.getTransacao() == null) {
			throw new MessageException("O campo Auditoria Entidade > Transação é obrigatório");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueNumeroDaOperacaoTransacaoEntidadeTipoRegistro(o);
		}
		validar2(o);
		validar3(o);
	}

	public void validarUniqueNumeroDaOperacaoTransacaoEntidadeTipoRegistro(AuditoriaEntidade o) {
		AuditoriaEntidadeSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.numeroDaOperacao().eq(o.getNumeroDaOperacao());
		select.transacao().eq(o.getTransacao());
		select.entidade().eq(o.getEntidade());
		select.tipo().eq(o.getTipo());
		select.registro().eq(o.getRegistro());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("auditoriaentidade_numerodaoperacao_transacao_entidade_tipo_registro"));
		}
	}

	protected void validar2(AuditoriaEntidade o) {}

	protected void validar3(AuditoriaEntidade o) {}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, AuditoriaEntidade> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		AuditoriaEntidadeSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<AuditoriaEntidade> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public AuditoriaEntidadeSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		AuditoriaEntidadeSelect<?> select = select(null);
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.fk(params, "entidade", select.entidade());
		FiltroConsulta.integer(params, "numeroDaOperacao", select.numeroDaOperacao());
		FiltroConsulta.integer(params, "registro", select.registro());
		FiltroConsulta.fk(params, "tipo", select.tipo());
		FiltroConsulta.fk(params, "transacao", select.transacao());
		return select;
	}

	@Override
	protected AuditoriaEntidade buscaUnicoObrig(MapSO params) {
		AuditoriaEntidadeSelect<?> select = selectBase(null, null, params);
		AuditoriaEntidade o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um AuditoriaEntidade com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected AuditoriaEntidade setOld(AuditoriaEntidade o) {
		AuditoriaEntidade old = newO();
		old.setEntidade(o.getEntidade());
		old.setNumeroDaOperacao(o.getNumeroDaOperacao());
		old.setRegistro(o.getRegistro());
		old.setTipo(o.getTipo());
		old.setTransacao(o.getTransacao());
		o.setOld(old);
		return o;
	}

	public AuditoriaEntidadeSelect<?> select(Boolean excluido) {
		AuditoriaEntidadeSelect<?> o = new AuditoriaEntidadeSelect<AuditoriaEntidadeSelect<?>>(null, super.criterio(), null);
		return o;
	}

	public AuditoriaEntidadeSelect<?> select() {
		return select(false);
	}

	@Override
	public String getText(AuditoriaEntidade o) {
		if (Null.is(o)) {
			return null;
		}
		return entidadeService.getText(o.getEntidade());
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("AuditoriaEntidade");
		list.add("transacao;entidade;tipo;registro;numeroDaOperacao");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("auditoriaentidade_numerodaoperacao_transacao_entidade_tipo_registro", "A combinação de campos Número da Operação + Transação + Entidade + Tipo + Registro não pode se repetir. Já existe um registro com esta combinação.");
	}
}
