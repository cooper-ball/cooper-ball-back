package br.cooper.infra.abstracoes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.cooper.infra.models.Login;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.LoginSelect;
import br.cooper.infra.services.UsuarioService;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringLength;

public abstract class LoginServiceAbstract extends ServiceModelo<Login> {

	@Autowired protected UsuarioService usuarioService;

	@Override
	public Class<Login> getClasse() {
		return Login.class;
	}

	@Override
	public MapSO toMap(Login o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		return map;
	}

	@Override
	protected Login fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		Login o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setData(mp.get("data"));
		o.setToken(mp.getString("token"));
		o.setUsuario(find(usuarioService, mp, "usuario"));
		return o;
	}

	@Override
	public Login newO() {
		Login o = new Login();
		return o;
	}

	@Override
	protected final void validar(Login o) {
		if (o.getData() == null) {
			throw new MessageException("O campo Login > Data é obrigatório");
		}
		o.setToken(tratarString(o.getToken()));
		if (o.getToken() == null) {
			throw new MessageException("O campo Login > Token é obrigatório");
		}
		if (StringLength.get(o.getToken()) > 50) {
			throw new MessageException("O campo Login > Token aceita no máximo 50 caracteres");
		}
		if (o.getUsuario() == null) {
			throw new MessageException("O campo Login > Usuário é obrigatório");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueToken(o);
			validarUniqueDataUsuario(o);
		}
		validar2(o);
		validar3(o);
	}

	public void validarUniqueToken(Login o) {
		LoginSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.token().eq(o.getToken());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("login_token"));
		}
	}

	public void validarUniqueDataUsuario(Login o) {
		LoginSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.data().eq(o.getData());
		select.usuario().eq(o.getUsuario());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("login_data_usuario"));
		}
	}

	protected void validar2(Login o) {}

	protected void validar3(Login o) {}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, Login> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		LoginSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<Login> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public LoginSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		LoginSelect<?> select = select(null);
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.dateTime(params, "data", select.data());
		FiltroConsulta.string(params, "token", select.token());
		FiltroConsulta.fk(params, "usuario", select.usuario());
		return select;
	}

	@Override
	protected Login buscaUnicoObrig(MapSO params) {
		LoginSelect<?> select = selectBase(null, null, params);
		Login o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um Login com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected Login setOld(Login o) {
		Login old = newO();
		old.setData(o.getData());
		old.setToken(o.getToken());
		old.setUsuario(o.getUsuario());
		o.setOld(old);
		return o;
	}

	public LoginSelect<?> select(Boolean excluido) {
		LoginSelect<?> o = new LoginSelect<LoginSelect<?>>(null, super.criterio(), null);
		return o;
	}

	public LoginSelect<?> select() {
		return select(false);
	}

	@Override
	public String getText(Login o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getToken();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Login");
		list.add("usuario;data;token");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("login_token", "O campo Token não pode se repetir. Já existe um registro com este valor.");
		CONSTRAINTS_MESSAGES.put("login_data_usuario", "A combinação de campos Data + Usuário não pode se repetir. Já existe um registro com esta combinação.");
	}
}
