package br.cooper.infra.abstracoes;

import java.util.List;

import javax.transaction.Transactional;

import br.cooper.infra.models.ArquivoExtensao;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.ArquivoExtensaoSelect;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;

public abstract class ArquivoExtensaoServiceAbstract extends ServiceModelo<ArquivoExtensao> {

	@Override
	public Class<ArquivoExtensao> getClasse() {
		return ArquivoExtensao.class;
	}

	@Override
	public MapSO toMap(ArquivoExtensao o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected ArquivoExtensao fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		ArquivoExtensao o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setNome(mp.getString("nome"));
		return o;
	}

	@Override
	public ArquivoExtensao newO() {
		ArquivoExtensao o = new ArquivoExtensao();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(ArquivoExtensao o) {
		o.setNome(tratarString(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Arquivo Extensão > Nome é obrigatório");
		}
		if (StringLength.get(o.getNome()) > 50) {
			throw new MessageException("O campo Arquivo Extensão > Nome aceita no máximo 50 caracteres");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueNome(o);
		}
		validar2(o);
		validar3(o);
	}

	public void validarUniqueNome(ArquivoExtensao o) {
		ArquivoExtensaoSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.nome().eq(o.getNome());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("arquivoextensao_nome"));
		}
	}

	protected void validar2(ArquivoExtensao o) {}

	protected void validar3(ArquivoExtensao o) {}

	@Transactional
	public ArquivoExtensao bloqueiaRegistro(ArquivoExtensao o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, ArquivoExtensao> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		ArquivoExtensaoSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<ArquivoExtensao> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public ArquivoExtensaoSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		ArquivoExtensaoSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.string(params, "nome", select.nome());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected ArquivoExtensao buscaUnicoObrig(MapSO params) {
		ArquivoExtensaoSelect<?> select = selectBase(null, null, params);
		ArquivoExtensao o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um ArquivoExtensao com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected ArquivoExtensao setOld(ArquivoExtensao o) {
		ArquivoExtensao old = newO();
		old.setNome(o.getNome());
		o.setOld(old);
		return o;
	}

	public ArquivoExtensaoSelect<?> select(Boolean excluido) {
		ArquivoExtensaoSelect<?> o = new ArquivoExtensaoSelect<ArquivoExtensaoSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public ArquivoExtensaoSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(ArquivoExtensao o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(ArquivoExtensao o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("ArquivoExtensao");
		list.add("nome");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("arquivoextensao_nome", "O campo Nome não pode se repetir. Já existe um registro com este valor.");
	}
}
