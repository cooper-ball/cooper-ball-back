package br.cooper.infra.abstracoes;

import java.util.List;

import javax.transaction.Transactional;

import br.cooper.infra.models.Arquivo;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.ArquivoSelect;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;

public abstract class ArquivoServiceAbstract extends ServiceModelo<Arquivo> {

	@Override
	public Class<Arquivo> getClasse() {
		return Arquivo.class;
	}

	@Override
	public MapSO toMap(Arquivo o, boolean listas) {
		setTransients(o);
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getNome() != null) {
			map.put("nome", o.getNome());
		}
		if (o.getTamanho() != null) {
			map.put("tamanho", o.getTamanho());
		}
		if (o.getType() != null) {
			map.put("type", o.getType());
		}
		if (o.getUri() != null) {
			map.put("uri", o.getUri());
		}
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected Arquivo fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		Arquivo o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setNome(mp.getString("nome"));
		o.setType(mp.getString("type"));
		o.setUri(mp.getString("uri"));
		setTransients(o);
		return o;
	}

	@Override
	public Arquivo newO() {
		Arquivo o = new Arquivo();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(Arquivo o) {
		o.setNome(tratarString(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Arquivo > Nome é obrigatório");
		}
		if (StringLength.get(o.getNome()) > 500) {
			throw new MessageException("O campo Arquivo > Nome aceita no máximo 500 caracteres");
		}
		validar2(o);
		o.setChecksum(tratarString(o.getChecksum()));
		if (o.getChecksum() == null) {
			throw new MessageException("O campo Arquivo > Checksum é obrigatório");
		}
		if (StringLength.get(o.getChecksum()) > 50) {
			throw new MessageException("O campo Arquivo > Checksum aceita no máximo 50 caracteres");
		}
		if (o.getExtensao() == null) {
			throw new MessageException("O campo Arquivo > Extensão é obrigatório");
		}
		if (o.getPath() == null) {
			throw new MessageException("O campo Arquivo > Path é obrigatório");
		}
		if (o.getTamanho() == null) {
			throw new MessageException("O campo Arquivo > Tamanho é obrigatório");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueChecksumExtensao(o);
		}
		validar3(o);
	}

	public void validarUniqueChecksumExtensao(Arquivo o) {
		ArquivoSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.checksum().eq(o.getChecksum());
		select.extensao().eq(o.getExtensao());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("arquivo_checksum_extensao"));
		}
	}

	protected void validar2(Arquivo o) {}

	protected void validar3(Arquivo o) {}

	@Transactional
	public Arquivo bloqueiaRegistro(Arquivo o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	public ResultadoConsulta consulta(MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, Arquivo> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		ArquivoSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<Arquivo> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public ArquivoSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		ArquivoSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.string(params, "checksum", select.checksum());
		FiltroConsulta.fk(params, "extensao", select.extensao());
		FiltroConsulta.string(params, "nome", select.nome());
		FiltroConsulta.fk(params, "path", select.path());
		FiltroConsulta.integer(params, "tamanho", select.tamanho());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected Arquivo buscaUnicoObrig(MapSO params) {
		ArquivoSelect<?> select = selectBase(null, null, params);
		Arquivo o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um Arquivo com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected Arquivo setOld(Arquivo o) {
		Arquivo old = newO();
		old.setChecksum(o.getChecksum());
		old.setExtensao(o.getExtensao());
		old.setNome(o.getNome());
		old.setPath(o.getPath());
		old.setTamanho(o.getTamanho());
		o.setOld(old);
		return o;
	}

	public ArquivoSelect<?> select(Boolean excluido) {
		ArquivoSelect<?> o = new ArquivoSelect<ArquivoSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public ArquivoSelect<?> select() {
		return select(false);
	}

	protected void setTransients(Arquivo o) {
		o.setType(getType(o));
		o.setUri(getUri(o));
	}

	protected abstract String getType(Arquivo o);

	protected abstract String getUri(Arquivo o);

	@Override
	protected void setBusca(Arquivo o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(Arquivo o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Arquivo");
		list.add("nome");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("arquivo_checksum_extensao", "A combinação de campos Checksum + Extensão não pode se repetir. Já existe um registro com esta combinação.");
	}
}
