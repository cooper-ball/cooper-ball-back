package br.cooper.infra.abstracoes;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import br.cooper.infra.models.ArquivoPath;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.ArquivoPathSelect;
import br.cooper.infra.services.ArquivoExtensaoService;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;

public abstract class ArquivoPathServiceAbstract extends ServiceModelo<ArquivoPath> {

	@Autowired protected ArquivoExtensaoService arquivoExtensaoService;

	@Override
	public Class<ArquivoPath> getClasse() {
		return ArquivoPath.class;
	}

	@Override
	public MapSO toMap(ArquivoPath o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected ArquivoPath fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		ArquivoPath o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setExtensao(find(arquivoExtensaoService, mp, "extensao"));
		o.setNome(mp.getString("nome"));
		return o;
	}

	@Override
	public ArquivoPath newO() {
		ArquivoPath o = new ArquivoPath();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(ArquivoPath o) {
		o.setNome(tratarString(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Arquivo Path > Nome é obrigatório");
		}
		if (StringLength.get(o.getNome()) > 50) {
			throw new MessageException("O campo Arquivo Path > Nome aceita no máximo 50 caracteres");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueNome(o);
		}
		validar2(o);
		if (o.getItens() == null) {
			throw new MessageException("O campo Arquivo Path > Itens é obrigatório");
		}
		validar3(o);
	}

	public void validarUniqueNome(ArquivoPath o) {
		ArquivoPathSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.nome().eq(o.getNome());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("arquivopath_nome"));
		}
	}

	protected void validar2(ArquivoPath o) {}

	protected void validar3(ArquivoPath o) {}

	@Transactional
	public ArquivoPath bloqueiaRegistro(ArquivoPath o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, ArquivoPath> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		ArquivoPathSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<ArquivoPath> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public ArquivoPathSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		ArquivoPathSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.fk(params, "extensao", select.extensao());
		FiltroConsulta.integer(params, "itens", select.itens());
		FiltroConsulta.string(params, "nome", select.nome());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected ArquivoPath buscaUnicoObrig(MapSO params) {
		ArquivoPathSelect<?> select = selectBase(null, null, params);
		ArquivoPath o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um ArquivoPath com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected ArquivoPath setOld(ArquivoPath o) {
		ArquivoPath old = newO();
		old.setExtensao(o.getExtensao());
		old.setItens(o.getItens());
		old.setNome(o.getNome());
		o.setOld(old);
		return o;
	}

	public ArquivoPathSelect<?> select(Boolean excluido) {
		ArquivoPathSelect<?> o = new ArquivoPathSelect<ArquivoPathSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public ArquivoPathSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(ArquivoPath o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(ArquivoPath o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("ArquivoPath");
		list.add("nome;extensao");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("arquivopath_nome", "O campo Nome não pode se repetir. Já existe um registro com este valor.");
	}
}
