package br.cooper.infra.abstracoes;

import java.util.List;

import javax.transaction.Transactional;

import br.cooper.infra.models.Entidade;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.EntidadeSelect;
import gm.utils.comum.Lst;
import gm.utils.comum.UBoolean;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;

public abstract class EntidadeServiceAbstract extends ServiceModelo<Entidade> {

	@Override
	public Class<Entidade> getClasse() {
		return Entidade.class;
	}

	@Override
	public MapSO toMap(Entidade o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected Entidade fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		Entidade o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setNome(mp.getString("nome"));
		o.setNomeClasse(mp.getString("nomeClasse"));
		o.setPrimitivo(UBoolean.toBoolean(mp.get("primitivo")));
		return o;
	}

	@Override
	public Entidade newO() {
		Entidade o = new Entidade();
		o.setPrimitivo(false);
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(Entidade o) {
		o.setNome(tratarString(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Entidade > Nome é obrigatório");
		}
		if (StringLength.get(o.getNome()) > 100) {
			throw new MessageException("O campo Entidade > Nome aceita no máximo 100 caracteres");
		}
		o.setNomeClasse(tratarString(o.getNomeClasse()));
		if (o.getNomeClasse() == null) {
			throw new MessageException("O campo Entidade > Nome Classe é obrigatório");
		}
		if (StringLength.get(o.getNomeClasse()) > 100) {
			throw new MessageException("O campo Entidade > Nome Classe aceita no máximo 100 caracteres");
		}
		if (o.getPrimitivo() == null) {
			throw new MessageException("O campo Entidade > Primitivo é obrigatório");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueNome(o);
			validarUniqueNomeClasse(o);
		}
		validar2(o);
		validar3(o);
	}

	public void validarUniqueNome(Entidade o) {
		EntidadeSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.nome().eq(o.getNome());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("entidade_nome"));
		}
	}

	public void validarUniqueNomeClasse(Entidade o) {
		EntidadeSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.nomeClasse().eq(o.getNomeClasse());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("entidade_nomeclasse"));
		}
	}

	protected void validar2(Entidade o) {}

	protected void validar3(Entidade o) {}

	@Transactional
	public Entidade bloqueiaRegistro(Entidade o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, Entidade> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		EntidadeSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<Entidade> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public EntidadeSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		EntidadeSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.string(params, "nome", select.nome());
		FiltroConsulta.string(params, "nomeClasse", select.nomeClasse());
		FiltroConsulta.bool(params, "primitivo", select.primitivo());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected Entidade buscaUnicoObrig(MapSO params) {
		EntidadeSelect<?> select = selectBase(null, null, params);
		Entidade o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um Entidade com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected Entidade setOld(Entidade o) {
		Entidade old = newO();
		old.setNome(o.getNome());
		old.setNomeClasse(o.getNomeClasse());
		old.setPrimitivo(o.getPrimitivo());
		o.setOld(old);
		return o;
	}

	public EntidadeSelect<?> select(Boolean excluido) {
		EntidadeSelect<?> o = new EntidadeSelect<EntidadeSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public EntidadeSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(Entidade o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(Entidade o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Entidade");
		list.add("nome;nomeClasse;primitivo");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("entidade_nome", "O campo Nome não pode se repetir. Já existe um registro com este valor.");
		CONSTRAINTS_MESSAGES.put("entidade_nomeclasse", "O campo Nome Classe não pode se repetir. Já existe um registro com este valor.");
	}
}
