package br.cooper.infra.outros;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.cooper.infra.models.Usuario;
import br.cooper.infra.services.LoginService;
import br.cooper.infra.services.UsuarioService;
import gm.utils.date.Data;
import gm.utils.email.EmailSender;
import gm.utils.email.UEmail;
import gm.utils.exception.MessageException;
import src.commom.utils.string.StringCompare;

@Component
public class EsqueciSenhaBo {

	@Autowired UsuarioService usuarioService;
	@Autowired LoginService loginService;
	@Autowired ApplicationProperties applicationProperties;

//	@Override
//	protected void persistInsert(EsqueciSenha o) {
//		Usuario u = usuarioService.find(loginService.find(ThreadScope.getLogin()).getUsuario().getId());
//		validarSenhas(o);
//		u.setSenha(o.getNovaSenha());
//		usuarioService.save(u);
//	}

//	private void validarSenhas(EsqueciSenha o) {
//		String a = o.getNovaSenha();
//		String b = o.getConfirmarSenha();
//		if (!StringCompare.eq(a, b)) {
//			throw new MessageException("Senhas não conferem!!");
//		}
//	}

	private String getCodigoRecuperacao(Usuario u) {
		String data = Data.hoje().format_dd_mm_yyyy();
		return UsuarioService.criptografarSenha(u, "recuperacao-"+data);
	}

	private Usuario getUsuario(String login) {
		Usuario u = usuarioService.findByLogin(login);
		if (u == null) {
			throw new MessageException("Usuário inválido!");
		}
		return u;
	}

	public void enviarEmail(String login) {

		if (!UEmail.isValid(login)) {
			throw new MessageException("E-email inválido: " + login);
		}

		try {

			Usuario u = getUsuario(login);
			String codigoRecuperacao = getCodigoRecuperacao(u);

			EmailSender mail = new EmailSender(applicationProperties.getEmailConta(), applicationProperties.getEmailSenha());

			mail.setAssunto("Código de recuperação");
			mail.setNomeRemetente("Sistem");
			mail.setTexto("Este é o código de recuperação de senha: " + codigoRecuperacao);
			mail.addDestinatario(login);
			mail.sendAssincrono(null, null);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public void confirmarCodigo(Usuario usuario, String codigo) {
		String esperado = getCodigoRecuperacao(usuario);
		if (!StringCompare.eq(codigo, esperado)) {
			throw new MessageException("Código inválido!");
		}
	}

}
