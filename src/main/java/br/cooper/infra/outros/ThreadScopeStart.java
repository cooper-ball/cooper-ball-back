package br.cooper.infra.outros;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.cooper.infra.models.Comando;
import br.cooper.infra.models.Entidade;
import br.cooper.infra.models.Login;
import br.cooper.infra.services.ComandoService;
import br.cooper.infra.services.EntidadeService;
import br.cooper.infra.services.LoginService;
import gm.utils.string.ListString;

@Component
public class ThreadScopeStart {

	@Autowired LoginService loginService;
	@Autowired ComandoService comandoService;
	@Autowired EntidadeService entidadeService;
	@Autowired AuditoriaEntidadeBo auditoriaEntidadeBo;

	public void start(Entidade entidade, String token, String nomeComando) {
		Comando comando = comandoService.get(entidade, nomeComando);
		Login login = loginService.valida(token, comando);
		start(login, comando);
	}

	public void start(Login login, Comando comando) {
		start(login.getId(), comando.getId());
	}

	public void start(int login, int comando) {
		if (ThreadScope.get() == null) {
			new ThreadScope(login, comando);
			ThreadScope.addOnSuccess(() -> auditoriaEntidadeBo.registrar());
		}
	}

	public void start(int loginId, String uri) {
		ListString list = ListString.byDelimiter(uri, "/").trimPlus();
		
		if (list.size() != 2) {
			throw new RuntimeException("uri invalida: " + uri);
		}
		
		String nomeEntidade = list.get(0);
		String nomeComando = list.get(1);
		Entidade entidade = entidadeService.select().nomeClasse().eq(nomeEntidade).uniqueObrig();
		Comando comando = comandoService.get(entidade, nomeComando);
		Login login = loginService.find(loginId);
		loginService.valida(login, comando);
		start(login, comando);
	}

}
