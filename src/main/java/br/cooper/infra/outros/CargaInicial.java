package br.cooper.infra.outros;

import br.cooper.ball.models.Telefone;
import br.cooper.ball.models.Torcedor;
import br.cooper.infra.models.Arquivo;
import br.cooper.infra.models.ArquivoExtensao;
import br.cooper.infra.models.ArquivoPath;
import br.cooper.infra.models.AuditoriaCampo;
import br.cooper.infra.models.AuditoriaEntidade;
import br.cooper.infra.models.AuditoriaTransacao;
import br.cooper.infra.models.Campo;
import br.cooper.infra.models.Comando;
import br.cooper.infra.models.ConsultaOperador;
import br.cooper.infra.models.Entidade;
import br.cooper.infra.models.Login;
import br.cooper.infra.models.Observacao;
import br.cooper.infra.models.Usuario;
import br.cooper.infra.services.EntidadeService;
import br.cooper.infra.services.UsuarioService;
import gm.utils.classes.ListClass;
import gm.utils.classes.UClass;
import java.lang.Class;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import src.commom.utils.object.Null;

@Component
public class CargaInicial {

	@Autowired EntidadeService entidadeService;
	@Autowired UsuarioService usuarioService;
	@Autowired CargaCampos cargaCampos;

	@PostConstruct
	public void init() {

		usuarioService.getSistema();
		usuarioService.add("usuario", "user@gmail.com");

		ListClass classes = new ListClass();

		classes.add(Arquivo.class);
		classes.add(ArquivoExtensao.class);
		classes.add(ArquivoPath.class);
		classes.add(AuditoriaCampo.class);
		classes.add(AuditoriaEntidade.class);
		classes.add(AuditoriaTransacao.class);
		classes.add(Campo.class);
		classes.add(Comando.class);
		classes.add(ConsultaOperador.class);
		classes.add(Entidade.class);
		classes.add(Login.class);
		classes.add(Observacao.class);
		classes.add(Usuario.class);
		classes.add(Telefone.class);
		classes.add(Torcedor.class);

		Map<Class<?>, Entidade> map = new HashMap<>();

		addEntidade(map, String.class);
		addEntidade(map, Integer.class);
		addEntidade(map, Long.class);
		addEntidade(map, Boolean.class);
		addEntidade(map, BigDecimal.class);
		addEntidade(map, Calendar.class);

		for (Class<?> classe : classes) {
			addEntidade(map, classe);
		}

		for (Class<?> classe : classes) {
			Entidade e = map.get(classe);
			cargaCampos.exec(e, classe);
		}
	}

	private void addEntidade(Map<Class<?>, Entidade> map, Class<?> classe) {

		String sn = classe.getSimpleName();
		Entidade o = entidadeService.select().nomeClasse().eq(sn).unique();

		if (Null.is(o)) {
			o = entidadeService.newO();
			o.setNome(UClass.getTitulo(classe));
			o.setNomeClasse(sn);
			o.setPrimitivo(false);
			o.setExcluido(false);
			o.setRegistroBloqueado(false);
			o.setBusca(o.getNomeClasse().toLowerCase());
			o = entidadeService.save(o);
		}

		map.put(classe, o);
	}
}
