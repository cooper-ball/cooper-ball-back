package br.cooper.infra.outros;

import java.util.List;

import gm.utils.map.MapSO;

public class ResultadoConsulta {
	public List<MapSO> dados;
	public Integer pagina;
	public Integer paginas;
	public Integer registros;
}