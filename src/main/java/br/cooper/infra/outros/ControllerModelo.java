package br.cooper.infra.outros;

import java.util.List;

import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import br.cooper.infra.models.AuditoriaEntidade;
import br.cooper.infra.models.Entidade;
import br.cooper.infra.models.TipoAuditoriaEntidade;
import br.cooper.infra.services.AuditoriaTransacaoService;
import br.cooper.infra.services.ComandoService;
import br.cooper.infra.services.EntidadeService;
import br.cooper.infra.services.LoginService;
import br.cooper.infra.services.ObservacaoService;
import br.cooper.infra.services.TipoAuditoriaEntidadeService;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.date.Data;
import gm.utils.lambda.FT;
import gm.utils.map.MapSO;

public abstract class ControllerModelo {

	@Autowired protected ThreadScopeStart startThreadScope;
	@Autowired protected ComandoService comandoService;
	@Autowired protected EntidadeService entidadeService;
	@Autowired protected LoginService loginService;
	@Autowired protected ObservacaoService observacaoService;
	@Autowired protected AuditoriaTransacaoService auditoriaTransacaoService;
	@Autowired protected AuditoriaEntidadeBo auditoriaEntidadeBo;
	@Autowired protected TipoAuditoriaEntidadeService tipoAuditoriaEntidadeService;
	protected abstract ServiceModelo<? extends EntityModelo> getService();

	protected ResponseEntity<Object> ok(String token, String comando, FT<Object> func) {
		return UController.ok(() -> {
			start(token, comando);
			return func.call();
		});
	}

	protected void start(String token, String comando) {
		startThreadScope.start(getEntidade(), token, comando);
	}

	@PutMapping
	public ResponseEntity<Object> save(@RequestHeader("token") String token, @RequestBody MapSO map) {
		return UController.ok(() -> {
//			MapSO map = MapSoFromJson.get(body);
			if (map.getInt("id") == null) {
				start(token, "insert");
			} else {
				start(token, "update");
			}
			return getService().saveAndMap(map);
		});
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> edit(@RequestHeader("token") String token, @PathParam("id") Integer id) {
		return ok(token, "edit", () -> getService().toMap(id, true));
	}

	public int getIdEntidade() {
		return getService().getIdEntidade();
	}

	public Entidade getEntidade() {
		return entidadeService.find(getIdEntidade());
	}

	@GetMapping("/consultar")
	public ResponseEntity<Object> consultar(@RequestHeader("token") String token, @RequestBody MapSO map) {
		return ok(token, "see", () -> getService().consulta(map));
	}

	@PostMapping("/consulta-select")
	public ResponseEntity<Object> consultaSelect(@RequestHeader("token") String token, @RequestBody MapSO map) {
		return ok(token, "see", () -> {
			map.put("busca", map.getString("text"));
			return getService().consultaSelect(map);
		});
	}

	@GetMapping("/auditoria/{id}")
	public ResponseEntity<Object> logs(@RequestHeader("token") String token, @PathParam("id") Integer id) {
		return ok(token, "see", () -> {
			Lst<AuditoriaEntidade> list = auditoriaEntidadeBo.list(getEntidade(), id);
			List<MapSO> lst = UList.map(list, o -> {
				MapSO mp = new MapSO();
				TipoAuditoriaEntidade tipo = tipoAuditoriaEntidadeService.find(o.getTipo());
				mp.put("id", o.getId());
				mp.put("idTipo", tipo.getId());
				mp.put("tipo", tipo.getNome());
				mp.put("usuario", o.getTransacao().getLogin().getUsuario().getNome());
				mp.put("data", new Data(o.getTransacao().getData()).format_dd_mm_yyyy_hh_mm_ss());
				mp.put("tempo", o.getTransacao().getTempo());
				return mp;
			});
			return lst;
		});
	}
	
	@DeleteMapping
	public ResponseEntity<Object> delete(@RequestHeader("token") String token, @PathParam("id") Integer id) {
		return ok(token, "delete", () -> {
			getService().delete(id);
			return new MapSO().add("message", "Registro Excluído com Sucesso!");
		});
	}

}
