package br.cooper.infra.selects;

import br.cooper.infra.models.AuditoriaEntidade;
import gm.utils.jpa.criterions.Criterio;
import gm.utils.jpa.select.SelectBase;
import gm.utils.jpa.select.SelectInteger;

public class AuditoriaEntidadeSelect<ORIGEM> extends SelectBase<ORIGEM, AuditoriaEntidade, AuditoriaEntidadeSelect<ORIGEM>> {

	public AuditoriaEntidadeSelect(ORIGEM origem, Criterio<?> criterio, String prefixo) {
		super(origem, criterio, prefixo, AuditoriaEntidade.class);
	}

	public SelectInteger<AuditoriaEntidadeSelect<?>> id() {
		return new SelectInteger<>(this, "id");
	}

	public AuditoriaTransacaoSelect<AuditoriaEntidadeSelect<?>> transacao() {
		return new AuditoriaTransacaoSelect<>(this, getC(), getPrefixo() + ".transacao" );
	}

	public EntidadeSelect<AuditoriaEntidadeSelect<?>> entidade() {
		return new EntidadeSelect<>(this, getC(), getPrefixo() + ".entidade" );
	}

	public SelectInteger<AuditoriaEntidadeSelect<?>> tipo() {
		return new SelectInteger<>(this, "tipo");
	}

	public SelectInteger<AuditoriaEntidadeSelect<?>> registro() {
		return new SelectInteger<>(this, "registro");
	}

	public SelectInteger<AuditoriaEntidadeSelect<?>> numeroDaOperacao() {
		return new SelectInteger<>(this, "numeroDaOperacao");
	}

	public AuditoriaEntidadeSelect<?> asc() {
		return id().asc();
	}
}
