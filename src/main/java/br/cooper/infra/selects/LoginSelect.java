package br.cooper.infra.selects;

import br.cooper.infra.models.Login;
import gm.utils.jpa.criterions.Criterio;
import gm.utils.jpa.select.SelectBase;
import gm.utils.jpa.select.SelectDate;
import gm.utils.jpa.select.SelectInteger;
import gm.utils.jpa.select.SelectString;

public class LoginSelect<ORIGEM> extends SelectBase<ORIGEM, Login, LoginSelect<ORIGEM>> {

	public LoginSelect(ORIGEM origem, Criterio<?> criterio, String prefixo) {
		super(origem, criterio, prefixo, Login.class);
	}

	public SelectInteger<LoginSelect<?>> id() {
		return new SelectInteger<>(this, "id");
	}

	public UsuarioSelect<LoginSelect<?>> usuario() {
		return new UsuarioSelect<>(this, getC(), getPrefixo() + ".usuario" );
	}

	public SelectDate<LoginSelect<?>> data() {
		return new SelectDate<>(this, "data");
	}

	public SelectString<LoginSelect<?>> token() {
		return new SelectString<>(this, "token");
	}

	public LoginSelect<?> asc() {
		return id().asc();
	}
}
