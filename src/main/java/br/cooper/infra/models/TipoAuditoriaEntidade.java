package br.cooper.infra.models;

import br.cooper.infra.outros.EntityModelo;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TipoAuditoriaEntidade extends EntityModelo {

	private final Integer id;

	private final String nome;

	@Override
	public void setId(Integer id) {
		throw new RuntimeException("???");
	}

	@Override
	public Boolean getExcluido() {
		return false;
	}

	@Override
	public void setExcluido(Boolean value) {}

	@Override
	public Boolean getRegistroBloqueado() {
		return false;
	}

	@Override
	public void setRegistroBloqueado(Boolean value) {}

	@Override
	public TipoAuditoriaEntidade getOld() {
		return (TipoAuditoriaEntidade) super.getOld();
	}

	public TipoAuditoriaEntidade(int id, String nome) {
		this.id = id;
		this.nome = nome;
	}
}
