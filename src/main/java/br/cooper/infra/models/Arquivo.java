package br.cooper.infra.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.cooper.infra.outros.EntityModelo;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Entity @Table(name = "arquivo")
public class Arquivo extends EntityModelo {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 500, name = "nome", nullable = false)
	private String nome;

	@Column(name = "tamanho", nullable = false)
	private Integer tamanho;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "path", nullable = false)
	private ArquivoPath path;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "extensao", nullable = false)
	private ArquivoExtensao extensao;

	@Column(length = 50, name = "checksum", nullable = false)
	private String checksum;

	@Column(name = "excluido", nullable = false)
	private Boolean excluido;

	@Column(name = "registrobloqueado", nullable = false)
	private Boolean registroBloqueado;

	@Column(length = 500, name = "busca", nullable = false)
	private String busca;

	@Transient
	private transient String uri;

	@Transient
	private transient String type;

	@Override
	public Arquivo getOld() {
		return (Arquivo) super.getOld();
	}
}
