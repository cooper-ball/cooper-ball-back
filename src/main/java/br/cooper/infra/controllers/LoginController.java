package br.cooper.infra.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.cooper.infra.outros.UController;
import br.cooper.infra.services.LoginService;
import gm.utils.map.MapSO;

@RestController @RequestMapping("/login")
public class LoginController {

	@Autowired LoginService loginService;

	@PostMapping("/signin")
	public ResponseEntity<Object> signin(@RequestBody MapSO map) {
		return UController.SUCESSO;
	}

}
