package br.cooper.infra.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.cooper.infra.abstracoes.LoginServiceAbstract;
import br.cooper.infra.models.Comando;
import br.cooper.infra.models.Login;
import br.cooper.infra.models.Usuario;
import gm.utils.comum.Aleatorio;
import gm.utils.date.Data;
import gm.utils.exception.MessageException;
import src.commom.utils.string.StringCompare;

@Component
public class LoginService extends LoginServiceAbstract {
	
	@Autowired UsuarioService usuarioService;

	private MessageException usuarioOuSenhaInvalida() {
		return new MessageException("Usuario ou senha invalida!");
	}

	@Transactional
	public Login exec(Usuario usuario) {
		Login o = newO();
		o.setUsuario(usuario);
		do {
			o.setToken(Aleatorio.getString(50));
		} while (select().token().eq(o.getToken()).exists());
		o.setData(Data.nowCalendar());
		insertSemAuditoria(o);
		return o;

	}

	@Transactional
	public Login logaSistema() {
		return exec(usuarioService.getSistema());
	}

	@Transactional
	public Login exec(String login, String senha) {
		Usuario usuario = usuarioService.findByLogin(login);
		if (usuario == null) {
			throw usuarioOuSenhaInvalida();
		}
		String senhaCriptografada = UsuarioService.criptografarSenha(usuario, senha);
		if (!StringCompare.eq(senhaCriptografada, usuario.getSenha())) {
			throw usuarioOuSenhaInvalida();
		}
		return exec(usuario);
	}

	@Transactional
	public Integer signin(String login, String senha) {
		return exec(login, senha).getId();
	}

	public Login valida(Login login, Comando comando) {
		//TODO colocar veriricacoes de permisoes aqui
		return login;
	}

	public Login valida(String token, Comando comando) {
		Login login = select().token().eq(token).uniqueObrig(() -> new MessageException("login nao encontrado: " + token));
		return valida(login, comando);
	}
	
}
