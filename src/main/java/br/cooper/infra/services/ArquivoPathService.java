package br.cooper.infra.services;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.cooper.infra.abstracoes.ArquivoPathServiceAbstract;
import br.cooper.infra.models.ArquivoExtensao;
import br.cooper.infra.models.ArquivoPath;
import br.cooper.infra.outros.ApplicationProperties;
import gm.utils.date.Data;

@Component
public class ArquivoPathService extends ArquivoPathServiceAbstract {

	@Autowired ApplicationProperties applicationProperties;

	public ArquivoPath get(ArquivoExtensao extensao) {
		ArquivoPath o = select().extensao().eq(extensao).itens().menor(1000).first();
		if (o == null) {
			o = newO();
			o.setExtensao(extensao);
			o.setItens(0);
			Data data = Data.now();
			String fileSystem = applicationProperties.getFileSystem();
			if (!fileSystem.endsWith("/")) {
				fileSystem += "/";
			}
			o.setNome(fileSystem + data.getAno() + "/" + extensao.getNome() + "/" + data.format("[mm][dd][yy][nn][ss]"));
			File diretorio = new File(o.getNome());
			if (!diretorio.exists()) {
				diretorio.mkdirs();
			}
			o = this.save(o);
		}
		return setOld(o);
	}

//	@Autowired private JmsTemplate jmsTemplate;
//
	public void incrementarItem(ArquivoPath o) {
//		this.jmsTemplate.convertAndSend("queue.sample", o.getId());
	}
//
//	@JmsListener(destination = "queue.sample")
//	public void onReceiverQueue(Integer id) {
//		ArquivoPath o = this.find(id);
//		o.setItens(o.getItens()+1);
//		this.save(o);
//	}

}
