package br.cooper.infra.services;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.security.MessageDigest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.cooper.infra.abstracoes.ArquivoServiceAbstract;
import br.cooper.infra.models.Arquivo;
import br.cooper.infra.models.ArquivoExtensao;
import br.cooper.infra.models.ArquivoPath;
import gm.utils.exception.UException;
import gm.utils.files.UFile;
import gm.utils.map.MapSO;
import gm.utils.number.UInteger;
import gm.utils.string.ListString;
import src.commom.utils.string.StringAfterFirst;
import src.commom.utils.string.StringAfterLast;
import src.commom.utils.string.StringEmpty;

@Component
public class ArquivoService extends ArquivoServiceAbstract {

	@Autowired ArquivoPathService arquivoPathService;
	@Autowired ArquivoExtensaoService arquivoExtensaoService;

	@Override
	public MapSO toMap(Arquivo o, boolean listas) {
		String extensao = o.getExtensao().getNome();
		MapSO map = super.toMap(o, listas);
		map.put("uri", "http://localhost:8080/arquivo/download/"+extensao+"/"+o.getChecksum()+"."+extensao);
		return map;
	}

	public Arquivo get(MapSO map) {

		if (map == null) {
			return null;
		}

		try {

			int id = map.id();
			if (id > 0) {
				return this.find(id);
			}

			String nome = map.getString("nome");

			String uri = map.getString("uri");

			String base64 = StringAfterFirst.get(uri, "base64,");
//			int tamanho = UInteger.toInt(base64.replace("=", "").length() * 0.75);
			byte[] bytes = java.util.Base64.getDecoder().decode(base64);

			return get(nome, bytes);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public Arquivo getArquivo(ListString list) {
		String nome;
		if (StringEmpty.is(list.getFileName())) {
			nome = "arquivo.txt";
		} else {
			nome = StringAfterLast.get("/" + list.getFileName(), "/");
		}
		return get(list, nome);
	}

	public Arquivo get(ListString list, String nome) {
		byte[] bytes = list.toString("\n").getBytes();
		return get(nome, bytes);
//		try {
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			DataOutputStream out = new DataOutputStream(baos);
//			for (String element : list) {
//			    out.writeUTF(element);
//			}
//			byte[] bytes = baos.toByteArray();
//			return get(nome, bytes);
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
	}

	public Arquivo get(String nome, byte[] bytes) {

		ArquivoExtensao extensao = arquivoExtensaoService.get(StringAfterFirst.get(nome, "."));

		String checksum = ArquivoService.getFileChecksum(bytes);

		Arquivo o = select().extensao().eq(extensao).checksum().eq(checksum).unique();
		if (o != null) return o;
		o = newO();
		o.setNome(nome);
		o.setChecksum(checksum);
		o.setExtensao(extensao);
		ArquivoPath arquivoPath = arquivoPathService.get(extensao);
		o.setPath(arquivoPath);
		o.setTamanho(bytes.length);

		String path = getFileName(o);
		if (!fileExists(o)) {
			try (OutputStream stream = new FileOutputStream(path)) {
				stream.write(bytes);
			} catch (Exception e) {
				throw UException.runtime(e);
			}
		}
		if (o.getTamanho() == 0) {
			long size = UFile.size(Paths.get(path));
			o.setTamanho(UInteger.toInt(size));
		}

		o = this.save(o);

		arquivoPathService.incrementarItem(arquivoPath);

		return o;
	}

	public boolean fileExists(Arquivo o) {
		return UFile.exists(getFileName(o));
	}

	private static String getFileChecksum(byte[] data) {

		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
			byte[] byteArray = new byte[1024];
			int count = 0;
			while ((count = byteArrayInputStream.read(byteArray)) != -1) {
				digest.update(byteArray, 0, count);
			}
			byteArrayInputStream.close();
			byte[] bytes = digest.digest();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public String getFileName(Arquivo o) {
		return o.getPath().getNome() + "/" + o.getChecksum() + "." + o.getExtensao().getNome();
	}

	@Override
	protected String getType(Arquivo o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getUri(Arquivo o) {
		// TODO Auto-generated method stub
		return null;
	}

}
