package br.cooper.infra.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.cooper.infra.abstracoes.ObservacaoServiceAbstract;
import br.cooper.infra.models.Entidade;
import br.cooper.infra.models.Observacao;
import gm.utils.map.MapSO;

@Component
public class ObservacaoService extends ObservacaoServiceAbstract {

	@Autowired EntidadeService entidadeService;

	public List<Observacao> get(Entidade entidade, int registro) {
		return select(null).entidade().eq(entidade).registro().eq(registro).list();
	}
	public List<Observacao> get(int entidade, int registro) {
		return this.get(entidadeService.find(entidade), registro);
	}
	public void save(int entidade, int registro, MapSO map) {
		List<MapSO> list = map.get("observacoes");
		if (list != null) {
			for (MapSO item : list) {
				item.add("entidade", entidade);
				item.add("registro", registro);
				this.save(item);
			}
		}
	}

	@Override
	protected Observacao fromMap(MapSO map) {
		Observacao o = super.fromMap(map);
		o.setEntidade(entidadeService.find(map.getIntObrig("entidade")));
		o.setRegistro(map.getIntObrig("registro"));
		return o;
	}
	public void add(int registro, int idEntidade, String msg) {
		Observacao o = newO();
		o.setRegistro(registro);
		o.setEntidade(entidadeService.find(idEntidade));
		o.setTexto(msg);
		save(o);
	}

}
