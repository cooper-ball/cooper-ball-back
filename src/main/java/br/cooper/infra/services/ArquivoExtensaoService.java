package br.cooper.infra.services;

import org.springframework.stereotype.Component;

import br.cooper.infra.abstracoes.ArquivoExtensaoServiceAbstract;
import br.cooper.infra.models.ArquivoExtensao;

@Component
public class ArquivoExtensaoService extends ArquivoExtensaoServiceAbstract {

	public ArquivoExtensao get(String nome) {
		ArquivoExtensao o = select().nome().eq(nome).unique();
		if (o == null) {
			o = newO();
			o.setNome(nome);
			o = this.save(o);
		}
		return o;
	}}
