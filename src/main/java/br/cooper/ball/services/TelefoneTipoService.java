package br.cooper.ball.services;

import br.cooper.ball.models.TelefoneTipo;
import br.cooper.infra.outros.ServiceModelo;
import gm.utils.comum.Lst;
import gm.utils.exception.MessageException;
import gm.utils.map.MapSO;
import java.lang.Class;
import java.util.HashMap;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.stereotype.Component;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringEmpty;

@Component
public class TelefoneTipoService extends ServiceModelo<TelefoneTipo> {

	public static final int RESIDENCIAL = 1;

	public static final int COMERCIAL = 2;

	public static final int CELULAR = 3;

	public static final Map<Integer, TelefoneTipo> MAP = new HashMap<>();

	@Override
	public Class<TelefoneTipo> getClasse() {
		return TelefoneTipo.class;
	}

	@Override
	public MapSO toMap(TelefoneTipo o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getNome() != null) {
			map.put("nome", o.getNome());
		}
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected TelefoneTipo fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		if (id == null || id < 1) {
			return null;
		} else {
			return find(id);
		}
	}

	@Override
	protected final void validar(TelefoneTipo o) {
		throw new MessageException("Nao deveria cair aqui pois o model estah anotado com @SemTabela");
	}

	@Transactional
	public TelefoneTipo bloqueiaRegistro(TelefoneTipo o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	protected TelefoneTipo buscaUnicoObrig(MapSO params) {
		String nome = params.getString("nome");
		Lst<TelefoneTipo> list = new Lst<TelefoneTipo>();
		list.addAll(MAP.values());
		TelefoneTipo o = list.unique(item -> {
			if (!StringEmpty.is(nome) && !StringCompare.eq(item.getNome(), nome)) {
				return false;
			}
			return true;
		});
		if (o == null) {
			throw new MessageException("Não foi encontrado um TelefoneTipo com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	static {
		MAP.put(RESIDENCIAL, new TelefoneTipo(RESIDENCIAL, "Residencial"));
		MAP.put(COMERCIAL, new TelefoneTipo(COMERCIAL, "Comercial"));
		MAP.put(CELULAR, new TelefoneTipo(CELULAR, "Celular"));
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	public String getText(TelefoneTipo o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public TelefoneTipo findNotObrig(int id) {
		return MAP.get(id);
	}
}
