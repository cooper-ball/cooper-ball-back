package br.cooper.ball.services;

import br.cooper.ball.models.Telefone;
import br.cooper.ball.selects.TelefoneSelect;
import br.cooper.infra.outros.CargaCampos;
import br.cooper.infra.services.AuditoriaCampoService;
import br.cooper.infra.outros.AuditoriaEntidadeBox;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import gm.utils.comum.Lst;
import gm.utils.comum.UBoolean;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import java.lang.Class;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;
import src.commom.utils.string.StringParse;

@Component
public class TelefoneService extends ServiceModelo<Telefone> {

	@Autowired TorcedorService torcedorService;
	@Autowired TelefoneTipoService telefoneTipoService;
	@Autowired AuditoriaCampoService auditoriaCampoService;

	@Override
	public Class<Telefone> getClasse() {
		return Telefone.class;
	}

	@Override
	public MapSO toMap(Telefone o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getNumero() != null) {
			map.put("numero", StringParse.get(o.getNumero()));
		}
		if (o.getPrincipal() != null) {
			map.put("principal", o.getPrincipal());
		}
		if (o.getTipo() != null) {
			map.put("tipo", telefoneTipoService.toIdText(o.getTipo()));
		}
		if (o.getTorcedor() != null) {
			map.put("torcedor", torcedorService.toIdText(o.getTorcedor()));
		}
		map.put("excluido", o.getExcluido());
		return map;
	}

	@Override
	protected Telefone fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		Telefone o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setNumero(mp.getString("numero"));
		o.setPrincipal(UBoolean.toBoolean(mp.get("principal")));
		o.setTipo(findId(telefoneTipoService, mp, "tipo"));
		o.setTorcedor(find(torcedorService, mp, "torcedor"));
		return o;
	}

	@Override
	public Telefone newO() {
		Telefone o = new Telefone();
		o.setPrincipal(false);
		o.setExcluido(false);
		return o;
	}

	@Override
	protected final void validar(Telefone o) {
		o.setNumero(tratarTelefone(o.getNumero()));
		if (o.getNumero() == null) {
			throw new MessageException("O campo Telefone > Número é obrigatório");
		}
		if (StringLength.get(o.getNumero()) > 50) {
			throw new MessageException("O campo Telefone > Número aceita no máximo 50 caracteres");
		}
		if (o.getPrincipal() == null) {
			throw new MessageException("O campo Telefone > Principal é obrigatório");
		}
		if (o.getTipo() == null) {
			throw new MessageException("O campo Telefone > Tipo é obrigatório");
		}
		if (o.getTorcedor() == null) {
			throw new MessageException("O campo Telefone > Torcedor é obrigatório");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueNumeroTorcedor(o);
			validarUniquePrincipalTorcedor(o);
		}
	}

	public void validarUniqueNumeroTorcedor(Telefone o) {
		TelefoneSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.numero().eq(o.getNumero());
		select.torcedor().eq(o.getTorcedor());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("telefone_numero_torcedor"));
		}
	}

	public void validarUniquePrincipalTorcedor(Telefone o) {
		if (o.getPrincipal() == null) return;
		TelefoneSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.principal().eq(o.getPrincipal());
		select.torcedor().eq(o.getTorcedor());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("telefone_principal_torcedor"));
		}
	}

	@Override
	public ResultadoConsulta consulta(MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, Telefone> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		TelefoneSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<Telefone> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public TelefoneSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		TelefoneSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.telefone(params, "numero", select.numero());
		FiltroConsulta.bool(params, "principal", select.principal());
		FiltroConsulta.fk(params, "tipo", select.tipo());
		FiltroConsulta.fk(params, "torcedor", select.torcedor());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		return select;
	}

	@Override
	protected Telefone buscaUnicoObrig(MapSO params) {
		TelefoneSelect<?> select = selectBase(null, null, params);
		Telefone o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um Telefone com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return true;
	}

	@Override
	protected Telefone setOld(Telefone o) {
		Telefone old = newO();
		old.setNumero(o.getNumero());
		old.setPrincipal(o.getPrincipal());
		old.setTipo(o.getTipo());
		old.setTorcedor(o.getTorcedor());
		o.setOld(old);
		return o;
	}

	@Override
	protected boolean houveMudancas(Telefone o) {
		Telefone old = o.getOld();
		if (!StringCompare.eq(o.getNumero(), old.getNumero())) {
			return true;
		}
		if (o.getPrincipal() != old.getPrincipal()) {
			return true;
		}
		if (o.getTipo() != old.getTipo()) {
			return true;
		}
		if (o.getTorcedor() != old.getTorcedor()) {
			return true;
		}
		return false;
	}

	@Override
	protected void registrarAuditoriaInsert(Telefone o) {
		AuditoriaEntidadeBox.novoInsert(getIdEntidade(), o.getId());
	}

	@Override
	protected void registrarAuditoriaUpdate(Telefone o) {
		int e = getIdEntidade();
		AuditoriaEntidadeBox a = AuditoriaEntidadeBox.novoUpdate(e, o.getId());
		Telefone old = o.getOld();
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "numero"), old.getNumero(), o.getNumero());
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "principal"), old.getPrincipal(), o.getPrincipal());
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "tipo"), old.getTipo(), o.getTipo());
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "torcedor"), old.getTorcedor(), o.getTorcedor());
	}

	@Override
	protected void registrarAuditoriaDelete(Telefone o) {
		AuditoriaEntidadeBox.novoDelete(getIdEntidade(), o.getId());
	}

	@Override
	protected void registrarAuditoriaUndelete(Telefone o) {
		AuditoriaEntidadeBox.novoUndelete(getIdEntidade(), o.getId());
	}

	@Override
	protected void registrarAuditoriaBloqueio(Telefone o) {
		AuditoriaEntidadeBox.novoBloqueio(getIdEntidade(), o.getId());
	}

	@Override
	protected void registrarAuditoriaDesbloqueio(Telefone o) {
		AuditoriaEntidadeBox.novoDesbloqueio(getIdEntidade(), o.getId());
	}

	public TelefoneSelect<?> select(Boolean excluido) {
		TelefoneSelect<?> o = new TelefoneSelect<TelefoneSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public TelefoneSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(Telefone o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(Telefone o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNumero();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Telefone");
		list.add("torcedor;numero;tipo;principal");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("telefone_numero_torcedor", "A combinação de campos Número + Torcedor não pode se repetir. Já existe um registro com esta combinação.");
		CONSTRAINTS_MESSAGES.put("telefone_principal_torcedor", "A combinação de campos Principal + Torcedor não pode se repetir. Já existe um registro com esta combinação.");
	}
}
