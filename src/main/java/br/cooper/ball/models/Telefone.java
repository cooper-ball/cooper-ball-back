package br.cooper.ball.models;

import br.cooper.infra.outros.EntityModelo;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Entity @Table(name = "telefone")
public class Telefone extends EntityModelo {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "torcedor", nullable = false)
	private Torcedor torcedor;

	@Column(length = 20, name = "numero", nullable = false)
	private String numero;

	@Column(name = "tipo", nullable = false)
	private Integer tipo;

	@Column(name = "principal", nullable = false)
	private Boolean principal;

	@Column(name = "excluido", nullable = false)
	private Boolean excluido;

	@Column(length = 500, name = "busca", nullable = false)
	private String busca;

	@Override
	public Boolean getRegistroBloqueado() {
		return false;
	}

	@Override
	public void setRegistroBloqueado(Boolean value) {}

	@Override
	public Telefone getOld() {
		return (Telefone) super.getOld();
	}
}
